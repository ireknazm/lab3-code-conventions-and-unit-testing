package com.hw.db.controllers;

import org.mockito.MockedStatic;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;

import java.util.Collections;
import java.util.List;

class threadControllerTests {
    private Thread thread;

    private static List<Post> posts = Collections.emptyList();

    private static com.hw.db.controllers.threadController threadController = new threadController();
    private static MockedStatic<ThreadDAO> threadDaoMocked = Mockito.mockStatic(ThreadDAO.class);
    private static MockedStatic<UserDAO> userDaoMocked = Mockito.mockStatic(UserDAO.class);

    @BeforeEach
    @DisplayName("Before each")
    void init() {
        long time = System.currentTimeMillis();

        this.thread = new Thread(
                "Irek",
                new Timestamp(time),
                "Irek's forum",
                "I am a tatar boy",
                "sccra",
                "Click me if not gay",
                11);

        threadDaoMocked.reset();
        userDaoMocked.reset();
    }

    @AfterAll
    @DisplayName("After all")
    static void clean(){
        threadDaoMocked.close();
        userDaoMocked.close();
    }

    @Test
    @DisplayName("Tests slug determination")
    void checkIdOrSlugSlugTest() {
        threadDaoMocked.when(
                () -> ThreadDAO.getThreadBySlug("sccra")
        ).thenReturn(thread);

        assertEquals(thread, threadController.CheckIdOrSlug("sccra"));
        assertNull(threadController.CheckIdOrSlug("pa-pa"));
    }

    @Test
    @DisplayName("Tests id determination")
    void checkIdOrSlugIdTest() {
        Integer id = 228;

        threadDaoMocked.when(
                () -> ThreadDAO.getThreadById(id)
        ).thenReturn(thread);

        assertEquals(thread, threadController.CheckIdOrSlug(id + ""));
        assertNull(threadController.CheckIdOrSlug("1488"));
    }

    @Test
    @DisplayName("Tests a post creation")
    void createPostTest() {
        assertEquals(
                ResponseEntity.status(HttpStatus.CREATED).body(posts),
                threadController.createPost("sccra-pa-opa", posts)
        );
    }

    @Test
    @DisplayName("Tests posts gaining")
    void postsTest() {
        threadDaoMocked.when(
                () -> ThreadDAO.getThreadBySlug("mans not hot")
        ).thenReturn(thread);

        assertEquals(
                ResponseEntity.status(HttpStatus.OK).body(posts),
                threadController.Posts("mans not hot", 10, 0, "sorting", false)
        );
    }

    @Test
    @DisplayName("Test the change")
    void changeTest() {
        threadDaoMocked.when(
                () -> ThreadDAO.getThreadBySlug("meme")
        ).thenReturn(thread);

        assertThrows(
                NullPointerException.class,
                () -> threadController.change("meme", thread)
        );
    }

    @Test
    @DisplayName("Test the info")
    void infoTest() {
        threadDaoMocked.when(
                () -> ThreadDAO.getThreadBySlug("kek")
        ).thenReturn(thread);

        assertEquals(
                ResponseEntity.status(HttpStatus.OK).body(thread),
                threadController.info("kek")
        );
    }

    @Test
    @DisplayName("Tests the vote creation")
    void createVoteTest() {
        User user = new User("Lesha", "leha@navalny.free", "Alexey Buzoterniy", "Detective");
        Vote vote = new Vote("Lesha", 10);

        userDaoMocked.when(
                () -> UserDAO.Info("Lesha")
        ).thenReturn(user);
        threadDaoMocked.when(
                () -> ThreadDAO.getThreadBySlug("a")
        ).thenReturn(thread);
        assertEquals(
                ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(),
                threadController.createVote("a", vote).getStatusCode()
        );
    }
}
